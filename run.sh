#!/bin/bash
docker-compose build
docker-compose up -d
echo -e "the petclinic build was a success"

sleep 60

if ! curl -s localhost:8080 | grep petclinic.css
then
  mysqlcont=$(docker ps | grep mysql:1.0.0 | awk '{print $1}')
  petcont=$(docker ps | grep petclinic:1.0.0 | awk '{print $1}')
  docker rm -f $petcont $mysqlcont
  docker rmi $(docker -a -q)
  exit 1
else
  docker tag petclinic/petclinic:1.0.0 dockerreg.grads.al-labs.co.uk:5000/chris/petclinic:2.0.0
  docker push dockerreg.grads.al-labs.co.uk:5000/chris/petclinic:2.0.0
fi
